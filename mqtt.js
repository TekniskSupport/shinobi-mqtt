// Bridge between Shinobi and MQTT

const moment = require('moment');
const mqtt = require('mqtt');


module.exports = (s, shinobiConfig, lang, app, io) => {
  
  // default config
  let config = {
    verbose: false,
    url: 'mqtt://localhost:1883',
    topic: 'shinobi',
    mqtt_options: {},
    toMqttMessage: {
      key: "key",
      name: "name",
      details: "details",
      currentTimestamp: "currentTimestamp",
      plug: "plug",
      name: "mon.name"
    }
    //TODO
    // trigger: true,
    // no_trigger: true,
  };

  // read global config and overwrite default config
  for (var key of Object.keys(shinobiConfig.mqtt || {})) {
    config[key] = shinobiConfig.mqtt[key];
  }

  const timestamp = () => moment().format('YYYY-MM-DD HH:mm:ss');
  const log = (wat) => console.log(`${timestamp()} MQTT:`, wat);
  const verboseLog = (wat) => {
    if(config.verbose) {
      console.log(`${timestamp()} MQTT:`, wat);
    }
  };
  
  // set mqtt options
  config.mqtt_options.clientId = config.mqtt_options.clientId || `shinobi_${Math.random().toString(16).substr(2, 8)}`;
  config.mqtt_options.reconnectPeriod = config.mqtt_options.reconnectPeriod || 1000; // 10 seconds

  // connect to mqtt server
  let mqttClient = mqtt.connect(config.url, config.mqtt_options);

  mqttClient.on('connect', () => {
    log('Connected to broker');
    if (typeof config.triggerTopics != 'undefined') {
      for (var topic of Object.keys(config.triggerTopics)) {
        verboseLog("found trigger topic to subscribe to: " + topic);
        verboseLog(config.triggerTopics[topic]);
        mqttClient.subscribe(topic, function (err) {
          if (!err) {
            log("subscribed to topic " + topic);
          } else log( "error subscribing: " + err );
        });
      };
    }
  });


  mqttClient.on('reconnect', () => log('Trying to reconnect to broker...'));
  mqttClient.on('disconnect', () => log('Disconnected from broker'));
  mqttClient.on('offline', () => log('Gone offline'));
  mqttClient.on('error', (e) => log(e));

  //handle incoming messages
  mqttClient.on('message',function(topic, message, packet){
    verboseLog("message received on topic " + topic + " with data " + message);
    let details = {};
    try {
      details = JSON.parse(message);
    } catch(e) {
      verboseLog("error parsing message from topic: " + e);
      return;
    }
    if (config.triggerTopics[topic] !== undefined) {
      s.triggerEvent({
        f: 'trigger',
        id: config.triggerTopics[topic]["monitorid"],
        ke: config.triggerTopics[topic]["groupkey"],
        details: details
       })
    }
  });


  function motionEventHandler(d, filter){
    verboseLog(`${d.f} event on ${d.id}`);
    if(!mqttClient.connected) {
      verboseLog('No connection to broker, skipping');
      return;
    }

    const userGroupKey = d.ke;
    const monitorId = d.id || d.mon.mid;
    if (config.filterplugs && config.filterplugs.indexOf(d.details.plug) > -1) {
      // filter out this event
      verboseLog('Event with plug: ' + d.details.plug + ' filtered out');
      return;
    }

    var evt = {};

    // Read from config which data elements to include and add them to 'evt'
    for (var dataElemnt of Object.keys(config.toMqttMessage) || {}) {
      var dataElemnts = config.toMqttMessage[dataElemnt].split('.');
      dataElemnts.forEach(function(element, index) { 
        if (index === 0) {
            evt[dataElemnt] = d[element];
        } else {
            evt[dataElemnt] = evt[dataElemnt][element];
        }
      });
    }
    const topic = `${config.topic}/${userGroupKey}/${monitorId}/${d.f}`;
    mqttClient.publish(topic, JSON.stringify(evt), null, () => {
      verboseLog(`Sent ${d.f} event #${d.id} to ${topic}`);
    });
  }

  s.onEventTrigger(motionEventHandler);
  s.onProcessExit(() => mqttClient.end());
  log('Bridge loaded');
};